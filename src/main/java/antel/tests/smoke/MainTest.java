package antel.tests.smoke;

import antel.pages.GlobalPage;
import automationFramework.tests.BaseTest;
import automationFramework.utils.WebDriverUtils;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MainTest extends BaseTest {


    GlobalPage globalPage;

    @Test(priority=1,description="Nombre Escenario - (Escenario: TEST_NAME_01)")
    public void test_01(){

        rep_logger = extent.createTest("TEST_NAME_01");

        try {

            rep_logger.log(Status.INFO,"Nomnbre del paso en el reporte");
            globalPage = PageFactory.initElements(driver, GlobalPage.class);
            globalPage.todoSomeMethod("Value");

        } catch (NoSuchElementException e) {

            rep_logger.log(Status.FAIL,"No se encontró Elemento: "+e.getMessage());
            WebDriverUtils.printScreen(driver,rep_logger);
            Assert.fail("No se encontró Elemento: "+e.getMessage());

        }

    }



}
