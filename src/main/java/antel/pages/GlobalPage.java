package antel.pages;

import automationFramework.pageObjects.BasePage;
import automationFramework.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class GlobalPage extends BasePage {

    @FindBy(how = How.XPATH, using = "//*[contains(@class,'v-panel-captionwrap')]//*[contains(text(), \"Workspace\")]/ancestor::node()[3]//*[contains(@class,'icon-button-caption')]/ancestor::node()[2]")
    @CacheLookup
    private WebElement webElementLocator1;


    private String webElementLocator2 = "//*[contains(@class,'v-window-header')][contains(text(), \"Workspace\")]//ancestor::node()[2]//*[contains(@class,'v-grid-row')]/*[contains(text(), \"WS_SSS\")]/ancestor::node()[1]";
    private String global_page_core_tab_caption = "//*[contains(@class,'mode-core offer-icon-bookmark')][contains(text(), \"CAPTION_CORE_PAGE\")]";
    public static String global_page_current_ws = null;

    public GlobalPage(WebDriver driver) {
    super(driver);
    }

    public GlobalPage todoSomeMethod(String something){

        //TODO
        //Agregar alguna funcionalidad al elemento click, drag, scroll, etc
        webElementLocator1.click();
        Utils.wait(3);
        return PageFactory.initElements(driver, GlobalPage.class);
    }


}
