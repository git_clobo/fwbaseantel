package automationFramework.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {

  private static final String loader = ".v-loading-indicator";

  public static String applyDefaultIfMissing(String variable, String defaultValue) {
    if (variable == null) {
      variable = defaultValue;
      System.out.println("Default " + defaultValue + " execution was applied since was not provided");
    }

    return variable;
  }

  public static String today() {
    Date today = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    return dateFormat.format(today);
  }

  public static String now() {
    Date today = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
    return dateFormat.format(today);
  }

  public static String nowWS() {
    Date today = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    return dateFormat.format(today);
  }

  public static void waitForPageLoadComplete(WebDriver driver, int specifiedTimeout) {
    Wait<WebDriver> wait = new WebDriverWait(driver, specifiedTimeout);
    wait.until(driver1 -> String
        .valueOf(((JavascriptExecutor) driver1).executeScript("return document.readyState"))
        .equals("complete"));
  }

  public static WebElement waitUntilLoadElement(WebDriver driver, WebElement element) {

    WebDriverWait waitLoaderVisible = new WebDriverWait(driver,Constants.PAGELOAD_TIMEOUT);
    WebDriverWait waitClick = new WebDriverWait(driver,Constants.PAGELOAD_TIMEOUT);

    // Tiempo de espera para la activación del loader
    Utils.wait(3);
    waitLoaderVisible.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(loader)));
    waitClick.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(element)));

    return element;

  }

  public static WebElement waitUntilVisible(WebDriver driver, WebElement element) {

    WebDriverWait waitVisible = new WebDriverWait(driver,Constants.PAGELOAD_TIMEOUT);
    waitVisible.until(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(element)));

    return element;

  }

  public static void wait(int time) {
    try {
      Thread.sleep(time * 1000);
    } catch (Exception e) {

    }

  }


}

