package automationFramework.tests;

import automationFramework.utils.GetProperties;
import automationFramework.utils.Utils;
import automationFramework.utils.datatypes.BrowserType;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

import static automationFramework.utils.Utils.applyDefaultIfMissing;

public class BaseTest {

    protected static WebDriver driver;
    private static String environment = applyDefaultIfMissing(System.getProperty("environment"), "PROD");
    protected static GetProperties properties = new GetProperties(environment);
    private static String browser = properties.getString("BROWSER").toUpperCase();
    protected LoginPage loginPage;
    ExtentHtmlReporter htmlReports;
    protected ExtentReports extent;
    protected ExtentTest rep_logger;

    String fileName = System.getProperty("user.dir") + "/test-output/" + Utils.now() + "-results.html";

    @BeforeTest
    public void setUp(){
        htmlReports = new  ExtentHtmlReporter(fileName);
        extent = new ExtentReports();
        extent.attachReporter(htmlReports);
        htmlReports.config().setDocumentTitle("Resultados Automation - Editor Ofertas Antel");
        htmlReports.config().setReportName("Testing de Regresión - Editor Ofertas Antel");
        htmlReports.config().setTheme(Theme.STANDARD);
        htmlReports.config().setTestViewChartLocation(ChartLocation.BOTTOM);
        htmlReports.config().setEncoding("utf-8");
        htmlReports.config().setCSS(".nav-wrapper{background-color:#0E1945;color:white} .brand-logo {background-image: url(http://vmlx-jbossrh-intg:18280/OfferEditorGUI/VAADIN/themes/offer-editor-theme-v1-5-16/img/logo.png);background-repeat: no-repeat;background-size: 60px 48px; color:#0E1945 !important; text-align: right;}");
        htmlReports.config().setJS("var logoElement = document.querySelector('.brand-logo');logoElement.removeAttribute('class');logoElement.setAttribute(\"class\", \"brand-logo\");logoElement.textContent = \"i\";");
    }

    @BeforeMethod
    public void setUp2(Method method) {
        BrowserType browserType = BrowserType.valueOf(browser.toUpperCase());
        FirefoxOptions ff_options;
        ChromeOptions chOptions;

        switch (browserType) {
            case FIREFOX:
                ff_options = new FirefoxOptions();
                ff_options.addPreference("dom.disable_open_during_load",false);
                //ff_options.addPreference("network.proxy.type", Proxy.ProxyType.AUTODETECT.ordinal());
                driver = new FirefoxDriver(ff_options);
                driver.manage().window().maximize();

                break;
            case CHROME:
                chOptions = new ChromeOptions();
                chOptions.addArguments("--disable-extensions");
                chOptions.addArguments("--start-maximized");
                chOptions.addArguments("--disable-popup-blocking");
                driver = new ChromeDriver(chOptions);

                break;

            default:
                ff_options = new FirefoxOptions();
                driver = new FirefoxDriver(ff_options);
                driver.manage().window().maximize();
        }

        navigateLogin();

    }

    private void navigateLogin() {
        String BASE_URL = properties.getString("BASE_URL");
        driver.get(BASE_URL);
        loginPage = PageFactory.initElements(driver,LoginPage.class);

        try {
            loginPage.signIn(properties.getString("USER"),
                             properties.getString("PASS"));
            Assert.assertTrue(loginPage.isElementPageDisplayed(loginPage.getHeader_logged_page()));
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            Assert.fail("Falla - navigateLogin()");
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Falla - Exception()");
        }

    }

    @AfterTest
    public void tearDown(){
        extent.flush();
    }

    @AfterMethod
    public void checkResults(ITestResult testResults){
        if(testResults.getStatus()==ITestResult.SUCCESS){
            rep_logger.log(Status.PASS, "Test Completado Exitosamente");
        }else{
            rep_logger.log(Status.FAIL, "Fallo el Test !");
            rep_logger.log(Status.FAIL, testResults.getThrowable());
        }
        driver.quit();
    }
}
